section .text

   
%define SYS_EXIT 60
%define MOVE_NUMERICAL 10
%define BUFF_SIZE 20
%define FOR_DOPOLNENIE -1


global exit
global string_length
global print_string
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

   
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
    	cmp byte [rdi+rax], 0
    	je .end
    	inc rax
    	jmp .loop
    .end:
    	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, r9
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rdi, '\n'
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov r8, rsp
    mov rax, rdi
    dec rsp
    mov byte [rsp], 0
    mov rdi, 10
    ;in loop will div until rax != 0
    .loop:
    	xor rdx, rdx
    	div rdi
    	mov rdi, rdx
    	add rdi, '0'
    	dec rsp
    	mov byte [rsp], dil
    	mov rdi, 10
    	cmp rax, 0
    	jnz .loop
    	mov rdi, rsp
    	call print_string
    	mov rsp, r8
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

string_equals:
    push rbx
    xor rdx, rdx
    .loop:
    	mov bl, byte[rdi+rdx]
    	mov cl, byte[rsi+rdx]
    	cmp bl, cl
    	jne .noteq
    	cmp bl, 0
    	je .equal
    	inc rdx
    	jmp .loop
    .noteq:
    	xor rax, rax
    	jmp .end
    .equal:
    	mov rax, 1
    	jmp .end
    .end:
    	pop rbx
    	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    mov rdx, 1
    mov rdi, 0
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
check_space:
    xor r9, r9
    cmp rax, ' '
    jz .space
    cmp rax, 0xA
    jz .space
    cmp rax, 0x9
    jz .space
    cmp rax, 0
    jz .end
    mov r9, 1
    .end:
    ret
    .space:
    mov r9, -1
    jmp .end


read_word:
    xor r8, r8
    .loop:
    	push r8
    	push rdi
    	push rsi
    	call read_char
    	pop rsi
    	pop rdi
    	pop r8
    	call check_space
    	cmp r9, -1
    	je .w_space
    	cmp r9, 0
    	je .end
    	mov [rdi+r8], rax
    	inc r8
    	cmp r8, rsi
    	jge .err
    	jmp .loop
    .w_space:
    	cmp r8, 0
    	je .loop
    	jmp .end
    .err:
    	xor rax, rax
    	xor rdx, rdx
    	ret
    .end:
    	xor rax, rax
    	mov [rdi + r8], rax
    	mov rax, rdi
    	mov rdx, r8
    	ret
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8 ; i
    mov r9, BUFF_SIZE ; buffer
    .loop:
    	mov r9b, byte[rdi+r8]
    	cmp r9, '0'
    	jb .break
    	cmp r9, '9'
    	ja .break
    	sub r9, '0'
    	imul rax, MOVE_NUMERICAL
    	add rax, r9
    	inc r8

    	jnz .loop
    	.break:
    	mov rdx, r8
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

parse_int:
    cmp byte[rdi], '-'
    jnz parse_uint
    inc rdi
    call parse_uint
    xor rax, FOR_DOPOLNENIE
    inc rax
    inc rdx
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    .loop:
    	cmp rcx, rdx
    	ja .too_long
    	mov r8, [rdi+rcx]
    	mov [rsi+rcx], r8
    	cmp r8, 0
    	je .compare
    	inc rcx
    	jmp .loop
    
    .compare:
    	mov rax, rcx
    	jmp .end
    	
    .too_long:
    	mov rax, 0
    
    .end:
    	ret
