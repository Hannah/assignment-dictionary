
extern string_equals
section .text

global find_word

;rdi - указатель на нуль-терминированную 
;rsi - указатель на начало словаря

find_word:
    	test rsi, rsi
    	jz .end
    	push rdi
    	push rsi
    	add rsi, 8
    	call string_equals
    	pop rsi
    	pop rdi
    	test rax, rax
    	jnz .end
    	mov rsi, [rsi]
    	jmp find_word
    	
    .end:
    	mov rax, rsi
    	ret
    
