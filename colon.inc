%define prev_elem 0
%macro colon 2
	%ifid %2
	    %2: dq prev_elem
	    %define prev_elem %2
	%else
		%error "Uncorrect mark"
	%endif
	%ifstr %1
		db %1, 0
	%else
		%error "Uncorrect key. It should be string"
	%endif
%endmacro
