%include "colon.inc"
%include "words.inc"
%include "lib.inc"

%define BUFFER 255
%define ERROR_MAGIC_CONST 13
%define ERROR_TREAD 2
%define MAIN_TREAD 1

global _start

extern find_word

section .rodata
	alert: db "Print key: ", 0
	allert_long_key: db "Key is too long", 0
	allert_non_ex_key: db "I dont know this key", 0

section .text

_start:
	mov rdi, alert
	mov rsi, 1
	call print_string
	sub rsp, BUFFER
	mov rsi, BUFFER
	mov rdi, rsp
	call read_word
	test rax, rax
	jz .len_err
	
	mov rsi, prev_elem
	mov rdi, rax
	call find_word 
	
	test rax, rax
	jz .cant_find
	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	inc rax
	add rdi, rax
	mov r9, MAIN_TREAD
	call print_string
	xor rdi, rdi
	jmp .end
	
.len_err:
	mov rdi, allert_long_key
	mov r9, ERROR_TREAD
	call print_string
	mov rdi, ERROR_MAGIC_CONST
	jmp .end
	
	
.cant_find:
	mov rdi, allert_non_ex_key
	mov r9, ERROR_TREAD
	call print_string
	mov rdi, ERROR_MAGIC_CONST
	
.end:
	add rsp, BUFFER
	call print_newline
	call exit	
